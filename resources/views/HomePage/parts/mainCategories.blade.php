<div class="container">
    <div class="row">
        <div class="col-md-3">
            <h3 class="subtitle">Chambre Bébé & Enfant</h3>
            <div class="banner banner-image">
                <a href="/chambre-bebes-enfants">
                    <img src="/assets/images/banners/banner-living.jpg" alt="banner">
                </a>
                <div class="banner-meta">
                    <span class="banner-price">Starting at <span>$999</span></span>
                </div><!-- End .banner-meta -->
            </div><!-- End .banner -->
        </div><!-- End .col-md-4 -->
        <div class="col-md-3">
            <h3 class="subtitle">Chambre Adulte</h3>
            <div class="banner banner-image">
                <a href="#">
                    <img src="/assets/images/banners/banner-bedroom.jpg" alt="banner">
                </a>
                <div class="banner-meta">
                    <span class="banner-price">à partir de  <span>20 DTN</span></span>
                </div><!-- End .banner-meta -->
            </div><!-- End .banner -->
        </div><!-- End .col-md-4 -->

        <div class="col-md-3">
            <h3 class="subtitle">Cuisine</h3>
            <div class="banner banner-image">
                <a href="#">
                    <img src="/assets/images/banners/banner-dining.jpg" alt="banner">
                </a>
                <div class="banner-meta">
                    <a href="#">DINING ROOM </a>

                    <span class="banner-price">Starting at <span>$859</span></span>
                </div><!-- End .banner-meta -->
            </div><!-- End .banner -->
        </div><!-- End .col-md-4 -->

        <div class="col-md-3">
            <h3 class="subtitle">Décoration</h3>
            <div class="banner banner-image">
                <a href="#">
                    <img src="/assets/images/banners/banner-bedroom.jpg" alt="banner">
                </a>
                <div class="banner-meta">
                    <a href="#">BEDROOM</a>

                    <span class="banner-price">Starting at <span>$888</span></span>
                </div><!-- End .banner-meta -->
            </div><!-- End .banner -->
        </div><!-- End .col-md-4 -->
    </div><!-- End .row -->
</div><!-- End .container -->