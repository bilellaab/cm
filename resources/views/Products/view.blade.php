@extends('layouts.layout')

@section('head')
    <meta property="og:type" content="product">
    <meta property="og:url" content="http://couture-maison.tn">
    <meta property="og:title" content="Cale bébé By couture maison">
    <meta property="og:site_name" content="atelier couture maison">
    <meta property="og:description" content="Le Cale bébé est idéal pour les premières nuits de bébé. Un nid douillet où bébé se sentira en Toute sécurité.  ">
    <meta property="og:image" content="{{$images['large']['0']}}">
@endsection


@section('content')

    <main class="main">
        @include('Shared.breadcrumb')
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="product-single-container product-single-default">
                        <div class="row">
                            <div class="col-lg-7 col-md-6 product-single-gallery">
                                <div class="product-slider-container product-item">
                                    <div class="product-single-carousel owl-carousel owl-theme">

                                        @foreach($images['large'] as $image)
                                        <div class="product-item">
                                            <figure class="product-image-container">
                                            <img class="product-single-image" src="{{$image}}" data-zoom-image="{{$image}}"/>
                                            @if (isset($specificPrice['percentage']))
                                                <span class="product-label label-sale" style="font-size: 2.0rem;">{{$specificPrice['percentage']}}</span>
                                            @endif
                                            </figure>
                                        </div>
                                        @endforeach

                                    </div>
                                    <!-- End .product-single-carousel -->
                                    <span class="prod-full-screen">
                                            <i class="icon-plus"></i>
                                        </span>
                                </div>
                                <div class="prod-thumbnail row owl-dots" id='carousel-custom-dots'>
                                    @foreach($images['small'] as $image)
                                        <div class="col-3 owl-dot">
                                            <img src="{{$image}}"/>
                                        </div>
                                    @endforeach

                                </div>
                            </div><!-- End .col-lg-7 -->

                            <div class="col-lg-5 col-md-6">
                                <div class="product-single-details">
                                    <h1 class="product-title">    {{$product["name"]["language"]}}  </h1>

                                    <div class="ratings-container">
                                        <div class="product-ratings">
                                            <span class="ratings" style="width:100%"></span><!-- End .ratings -->
                                        </div><!-- End .product-ratings -->

                                        <a href="#" class="rating-link d-none">( 6 Reviews )</a>
                                    </div><!-- End .product-container -->

                                    @include('Shared.price')

                                    <div class="product-desc">
                                        {!!  isset($product["description_short"]["language"]) ? $product["description_short"]["language"] : '' !!}
                                     </div><!-- End .product-desc -->

                                    <div class="sticky-header">
                                        <div class="container">
                                            <div class="sticky-img">
                                                <img src="{{$images['small']['0']}}" />
                                            </div>
                                            <div class="sticky-detail">
                                                <div class="sticky-product-name">
                                                    <h2 class="product-title">{{$product["name"]["language"]}}</h2>
                                                    @include('Shared.price')
                                                </div>
                                                <div class="ratings-container">
                                                    <div class="product-ratings">
                                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->
                                                    </div><!-- End .product-ratings -->

                                                    <a href="#" class="rating-link">  </a>
                                                </div><!-- End .product-container -->
                                            </div><!-- End .sticky-detail -->
                                            <a href="/cart" class="paction add-cart" >
                                                <span>J’achète !</span>
                                            </a>
                                        </div><!-- end .container -->
                                    </div><!-- end .sticky-header -->

                                    <div class="product-action product-all-icons">
                                        <a href="/cart" class="paction add-cart">
                                            <span>J’achète !</span>
                                        </a>
                                    </div><!-- End .product-action -->

                                    <div class="product-single-share">

                                        <!-- www.addthis.com share plugin-->
                                        <div class="sharethis-inline-share-buttons"></div>

                                    </div><!-- End .product single-share -->
                                </div><!-- End .product-single-details -->
                            </div><!-- End .col-lg-5 -->
                        </div><!-- End .row -->
                    </div><!-- End .product-single-container -->

                    <div class="product-single-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="product-tab-desc" data-toggle="tab" href="#product-desc-content" role="tab" aria-controls="product-desc-content" aria-selected="true">Description</a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel" aria-labelledby="product-tab-desc">
                                <div class="product-desc-content">
                                    {!! isset($product["description"]["language"]) ? $product["description"]["language"] : '' !!}
                                </div><!-- End .product-desc-content -->
                            </div><!-- End .tab-pane -->
                        </div><!-- End .tab-content -->
                    </div><!-- End .product-single-tabs -->
                </div><!-- End .col-lg-9 -->

                @include("Products.parts.sideBar")
            </div><!-- End .row -->
        </div><!-- End .container -->

        @include("Products.parts.featuredProductsCarousel")
    </main><!-- End .main -->


@endsection