<div class="sidebar-overlay"></div>
<aside class="sidebar-shop col-lg-3 order-lg-first">
    <div class="pin-wrapper" style="height: 1351px;">
        <div class="sidebar-wrapper" style="border-bottom: 0px none rgb(119, 119, 119); width: 270px;">
            <div class="widget">
                <h3 class="widget-title">
                    <a data-toggle="collapse" href="#widget-body-1" role="button" aria-expanded="true" aria-controls="widget-body-1">{{$categoryData ['name']['language']}}</a>
                </h3>
                @if (isset($subCategories) && count ($subCategories)>0)
                <div class="collapse show" id="widget-body-1">
                    <div class="widget-body">
                        <ul class="cat-list">

                                @foreach($subCategories as $cat)
                                <li><a href="{{$cat['link_rewrite']['language']}}">{{$cat['name']['language']}}</a></li>
                                @endforeach

                        </ul>
                    </div><!-- End .widget-body -->
                </div><!-- End .collapse -->
                @endif
            </div><!-- End .widget -->
            <div class="widget">
                <h3 class="widget-title">
                    <a data-toggle="collapse" href="#widget-body-6" role="button" aria-expanded="true" aria-controls="widget-body-6">Color</a>
                </h3>

                <div class="collapse show" id="widget-body-6">
                    <div class="widget-body">
                        <ul class="config-swatch-list">
                            <li>
                                <a href="#" style="background-color: #4090d5;"></a>
                            </li>
                            <li class="active">
                                <a href="#" style="background-color: #f5494a;"></a>
                            </li>
                            <li>
                                <a href="#" style="background-color: #fca309;"></a>
                            </li>
                            <li>
                                <a href="#" style="background-color: #11426b;"></a>
                            </li>
                            <li>
                                <a href="#" style="background-color: #f0f0f0;"></a>
                            </li>
                            <li>
                                <a href="#" style="background-color: #3fd5c9;"></a>
                            </li>
                            <li>
                                <a href="#" style="background-color: #979c1c;"></a>
                            </li>
                            <li>
                                <a href="#" style="background-color: #7d5a3c;"></a>
                            </li>
                        </ul>
                    </div><!-- End .widget-body -->
                </div><!-- End .collapse -->
            </div><!-- End .widget -->



            @include('shared.sideBarFeaturedProducts')


            <div class="widget widget-block">
                <h3 class="widget-title">Custom HTML Block</h3>
                <h5>This is a custom sub-title.</h5>
                <p>Lorem ipsum dolor sit amet, consectetur elitad adipiscing Cras non placerat mi. </p>
            </div><!-- End .widget -->
        </div>
    </div><!-- End .sidebar-wrapper -->
</aside><!-- End .col-lg-3 -->