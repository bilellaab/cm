

<div class="banner banner-cat" style="background-image: url('assets/images/banners/banner-2.jpg');">
    <div class="banner-content container">
        <h2 class="banner-subtitle">check out over <span>200+</span></h2>
        <h1 class="banner-title">
            INCREDIBLE deals
        </h1>
        <a href="#" class="btn btn-dark">Shop Now</a>
    </div><!-- End .banner-content -->
</div><!-- End .banner -->