<div class="row products-body">
    <div class="col-lg-12 main-content">
        <div class="row row-sm">
            @foreach ( (array) $categories as $category)
                <div class="col-6 col-md-4">
                    <div class="product">
                        <figure class="product-image-container">
                            <a href="/{{$category['link_rewrite']['language']}}" class="product-image">
                                <img src="https://via.placeholder.com/50" alt="category">
                            </a>
                            <a href="product.html" class="paction add-cart" title="Add to Cart">
                                <span>Explorer !</span>
                            </a>
                            <span class="product-label label-sale">-20%</span>
                        </figure>
                        <div class="product-details product-price-inner">
                            <div class="ratings-container">
                                <div class="product-ratings">
                                    <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                                </div><!-- End .product-ratings -->
                            </div><!-- End .product-container -->
                            <h2 class="product-title">
                                <a href="/{{$category['link_rewrite']['language']}}.html">{{$category['name']['language']}}</a>
                            </h2>
                        </div><!-- End .product-details -->
                    </div><!-- End .product -->
                </div><!-- End .col-xl-3 -->
            @endforeach
        </div><!-- End .row -->
</div><!-- End .row -->