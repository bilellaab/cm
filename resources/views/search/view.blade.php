@extends('layouts.layout')

@section('content')

    <main class="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include("Shared.listingslider")
                </div>
            </div>
            @include("Shared.breadcrumb")

            @if ((array) $categories)
            <div class="mb-1"></div><!-- margin -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="row row-sm">
                        <div class="col-6 col-md-4 col-xl-3">
                            @foreach ( (array) $categories as $category)
                                <a href="/{{$category['link_rewrite']['language']}}.html">{{$category['name']['language']}}</a> <br/>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <hr color="red">
            @endif
            <div class="row">
                @if (!(array) $products)
                    <div class="col-lg-12">
                        <div class="row row-sm">
                            <div class="col-6 col-md-4 col-xl-3">
                                no products match with your search
                            </div>
                        </div>
                    </div>
                @else
                <div class="col-lg-12">
                    <div class="row row-sm">
                        @foreach ($products as $product)
                        <div class="col-6 col-md-4 col-xl-3">
                                @include("Shared.listingProduct")
                        </div><!-- End .col-xl-3 -->
                        @endforeach
                    </div><!-- End .row -->
                 </div><!-- End .col-lg-12 -->
                @endif
                <!-- End .col-lg-3 -->
            </div><!-- End .row -->
        </div><!-- End .container -->

        <div class="mb-5"></div><!-- margin -->
    </main>




@endsection