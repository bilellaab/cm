<nav aria-label="breadcrumb" class="breadcrumb-nav">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/"><i class="icon-home"></i></a></li>
            @if (isset ($breadcrumbs) && count($breadcrumbs))
            @foreach ($breadcrumbs as $breadcrumb)
                @if ($breadcrumb['url'] && !$loop->last)
                <li class="breadcrumb-item"><a href="{{$breadcrumb['url']}}">{{$breadcrumb['title']}}</a></li>
                @else
                <li class="breadcrumb-item active">{{$breadcrumb['title']}}</li>
                @endif
            @endforeach
            @endif
        </ol>
    </div><!-- End .container -->
</nav>
<div class="mb-3"></div><!-- margin -->










