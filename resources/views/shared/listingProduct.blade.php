
                    <div class="product">
                        <figure class="product-image-container" style="background-color:white">
                            <a href="/{{$product['link_rewrite']['language']}}.html" class="product-image">
                                <img src="{{$cdn}}/{{ $product['id_default_image'] }}-medium_default/{{ $product['link_rewrite']['language'] }}.jpg" alt="product">
                            </a>
                            @if (isset($specificPrice[$product['id']]['percentage']))
                                <span class="product-label label-sale" style="font-size:1.4rem">{{$specificPrice[$product['id']]['percentage']}}</span>
                            @endif


                        </figure>
                        <div class="product-details product-price-inner">
                            <div class="ratings-container d-none">
                                <div class="product-ratings">
                                    <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                                </div><!-- End .product-ratings -->
                            </div><!-- End .product-container -->
                            <h2 class="product-title">
                                <a href="/{{$product['link_rewrite']['language']}}.html">{{$product['name']['language']}}</a>
                            </h2>
                            @if (isset($specificPrice[$product['id']]['newPrice']))
                                <div class="price-box">
                                    <span class="old-price">{{$product["price"]}} DT</span>
                                    <span class="product-price">{{$specificPrice[$product['id']]['newPrice']}} DT</span>
                                </div><!-- End .price-box -->
                            @else
                                <div class="price-box">
                                    <span class="product-price">{{$product["price"]}}  DT</span>
                                </div><!-- End .price-box -->
                            @endif
                        </div><!-- End .product-details -->
                    </div><!-- End .product -->

