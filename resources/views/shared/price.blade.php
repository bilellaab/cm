
@if (isset($specificPrice['newPrice']))
<div class="price-box">
    <span class="old-price">{{$product["price"]}} DT</span>
    <span class="product-price">{{$specificPrice['newPrice']}} DT</span>

</div><!-- End .price-box -->
@else
<div class="price-box">
    <span class="product-price">{{$product["price"]}} DT</span>
</div><!-- End .price-box -->
@endif