<!DOCTYPE html>
<html lang="en">

@include('layouts.parts.head')

<body>
<div class="page-wrapper">
    <header class="header">
        @include('layouts.parts.headerTop')
        @include('layouts.parts.headerMiddle')
        @include('layouts.parts.horizontalMainMenu')
    </header><!-- End .header -->

    @yield('content')

    @include('layouts.parts.footer')

</div><!-- End .page-wrapper -->

@include('layouts.parts.mobileMenu')

@include('layouts.parts.newsletter-popup')

<a id="scroll-top" href="#top" title="Top" role="button"><i class="icon-angle-up"></i></a>

<!-- Plugins JS File -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/plugins.min.js"></script>
<script src="/assets/js/jplist.min.js"></script>


<!-- Main JS File -->
<script src="/assets/js/main.min.js"></script>
<script>
    jplist.init();
</script>
</body>

</html>