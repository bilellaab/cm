<?php
$menu = [
        [
            'title' => 'Home',
            'url' => '/'
        ],
        [
            'title' => '',
            'url' => ''
        ]
];
?>


<div class="header-bottom sticky-header">
    <div class="container">
        <nav class="main-nav">
            <ul class="menu sf-arrows">
                <li class="active"><a href="{{$menu['0']['url']}}">{{$menu['0']['title']}}</a></li>
                <li>
                    <a href="/category" class="sf-with-ul">CHAMBRE ADULTE</a>
                    <div class="megamenu megamenu-fixed-width">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="menu-title">
                                            <a href="#">Paure de lit<span class="tip tip-new">New!</span></a>
                                        </div>
                                        <ul>
                                            <li><a href="category-banner-full-width.html">Fullwidth Banner<span class="tip tip-hot">Hot!</span></a></li>
                                        </ul>
                                        <div class="menu-title">
                                            <a href="#">Rideaux</a>
                                        </div>
                                        <ul>
                                            <li><a href="category-banner-full-width.html">Fullwidth Banner</a></li>
                                        </ul>
                                    </div><!-- End .col-lg-6 -->
                                    <div class="col-lg-6">
                                        <div class="menu-title">
                                            <a href="#">Variations 2</a>
                                        </div>
                                        <ul>
                                            <li><a href="#">Product List Item Types</a></li>
                                            <li><a href="category-infinite-scroll.html">Ajax Infinite Scroll</a></li>
                                        </ul>
                                    </div><!-- End .col-lg-6 -->
                                </div><!-- End .row -->
                            </div><!-- End .col-lg-8 -->
                            <div class="col-lg-4">
                                <div class="banner">
                                    <a href="#">
                                        <img src="/assets/images/menu-banner-2.jpg" alt="Menu banner">
                                    </a>
                                </div><!-- End .banner -->
                            </div><!-- End .col-lg-4 -->
                        </div>
                    </div><!-- End .megamenu -->
                </li>



                <li class="megamenu-container">
                    <a href="product.html" class="sf-with-ul">BÉBÉ & ENFANT</a>
                    <div class="megamenu">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="menu-title">
                                            <a href="#">Bébé</a>
                                        </div>
                                        <ul>
                                            <li><a href="product.html">Landau</a></li>
                                            <li><a href="product-sticky-tab.html">Sac maman</a></li>
                                            <li><a href="product-sticky-tab.html">Vanity</a></li>
                                            <li><a href="product-sticky-tab.html">Berceau</a></li>
                                            <li><a href="product-sticky-tab.html">Linge de lit</a></li>

                                        </ul>
                                    </div><!-- End .col-lg-4 -->
                                    <div class="col-lg-6">
                                        <div class="menu-title">
                                            <a href="#">Enfant</a>
                                        </div>
                                        <ul>
                                            <li><a href="product-sticky-tab.html">Sticky Tabs</a></li>
                                            <li><a href="product-sticky-tab.html">Sticky Tabs</a></li>
                                            <li><a href="product-sticky-tab.html">Sticky Tabs</a></li>
                                            <li><a href="product-sticky-tab.html">Sticky Tabs</a></li>
                                            <li><a href="product-sticky-tab.html">Sticky Tabs</a></li>

                                        </ul>
                                    </div><!-- End .col-lg-4 -->




                                </div><!-- End .row -->
                            </div><!-- End .col-lg-8 -->
                            <div class="col-lg-4">
                                <div class="banner">
                                    <a href="#">
                                        <img src="/assets/images/menu-banner.jpg" alt="Menu banner" class="product-promo">
                                    </a>
                                </div><!-- End .banner -->
                            </div><!-- End .col-lg-4 -->
                        </div><!-- End .row -->
                    </div><!-- End .megamenu -->
                </li>
                <li>
                    <a href="#" class="sf-with-ul">Pages</a>

                    <ul>
                        <li><a href="cart.html">Shopping Cart</a></li>
                        <li><a href="#">Checkout</a>
                            <ul>
                                <li><a href="checkout-shipping.html">Checkout Shipping</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <li class="float-right"><a href="#">Buy Porto!</a></li>
                <li class="float-right"><a href="#">Special Offer!</a></li>
            </ul>
        </nav>
    </div><!-- End .header-bottom -->
</div><!-- End .header-bottom -->