<div class="header-search">
    <a href="#" class="search-toggle" role="button"><i class="icon-magnifier"></i></a>
    <form action="/search/" method="get">
        <div class="header-search-wrapper">
            <input type="search" class="form-control" name="q" id="q" placeholder="Chercher un produit..." required>
            <button class="btn" type="submit"><i class="icon-magnifier"></i></button>
        </div><!-- End .header-search-wrapper -->
    </form>
</div><!-- End .header-search -->