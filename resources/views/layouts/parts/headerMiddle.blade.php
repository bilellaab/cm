<div class="header-middle">
    <div class="container">


        <div class="header-left">
            <a href="/" class="logo">
                <img src="/assets/images/logo.png" alt="Porto Logo">
            </a>
        </div><!-- End .headeer-center -->

        <div class="header-center">
            <button class="mobile-menu-toggler" type="button">
                <i class="icon-menu"></i>
            </button>
            @include("layouts.parts.searchFormHeaderMidle")

    </div><!-- End .header-left -->

        <div class="header-right">
            <div class="header-contact">
                <span>Call us now</span>
                <a href="tel:#"><strong>+123 5678 890</strong></a>
            </div><!-- End .header-contact -->
            @include("layouts.parts.cartDropdownHeaderMidle")

        </div><!-- End .header-right -->
    </div><!-- End .container -->
</div><!-- End .header-middle -->