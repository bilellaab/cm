<footer class="footer">
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="widget">
                                <h4 class="widget-title">SERVICE CLIENT</h4>
                                <ul class="links">
                                    <li><a href="about.html">Aide & FAQ</a></li>
                                    <li><a href="contact.html">Contactez-nous</a></li>
                                    <li><a href="#">Paiement</a></li>
                                    <li><a href="#">Livraisons</a></li>
                                    <li><a href="#" class="login-link">Login</a></li>
                                </ul>
                            </div><!-- End .widget -->
                        </div><!-- End .col-md-3 -->

                        <div class="col-md-3">
                            <div class="widget">
                                <h4 class="widget-title">A PROPOS</h4>
                                <ul class="links">
                                    <li><a href="about.html">Qui sommes-nous</a></li>
                                    <li><a href="contact.html">Contactez-nous</a></li>
                                    <li><a href="my-account.html">Politique de Confidentialité</a></li>
                                    <li><a href="#">Conditions d'utilisation</a></li>
                                </ul>
                            </div><!-- End .widget -->
                        </div><!-- End .col-md-3 -->

                        <div class="col-md-5">
                            <div class="widget">
                                <ul class="contact-info">
                                    <li>
                                        <span class="contact-info-label">Address:</span>123 Street Name, City, England
                                    </li>
                                    <li>
                                        <span class="contact-info-label">Phone:</span>Toll Free <a href="tel:">(123) 456-7890</a>
                                    </li>
                                    <li>
                                        <span class="contact-info-label">Email:</span> <a href="mailto:mail@example.com">mail@example.com</a>
                                    </li>
                                    <li>
                                        <span class="contact-info-label">Working Days/Hours:</span>
                                        Mon - Sun / 9:00AM - 8:00PM
                                    </li>
                                </ul>
                            </div><!-- End .widget -->
                        </div><!-- End .col-md-5 -->

                    </div><!-- End .row -->
                </div><!-- End .col-lg-8 -->

                <div class="col-lg-4">
                    <div class="widget widget-newsletter">
                        <h4 class="widget-title">Subscribe newsletter</h4>
                        <p>Get all the latest information on Events,Sales and Offers. Sign up for newsletter today</p>
                        <form action="#">
                            <input type="email" class="form-control" placeholder="Email address" required>

                            <input type="submit" class="btn" value="Subscribe">
                        </form>
                    </div><!-- End .widget -->
                </div><!-- End .col-lg-4 -->
            </div><!-- End .row -->
        </div><!-- End .container -->
    </div><!-- End .footer-middle -->

    <div class="container">
        <div class="footer-bottom">
            <p class="footer-copyright">Couture Maison. &copy;  2019.  All Rights Reserved</p>
            <div class="social-icons" style="margin-left: 85rem;">
                <a href="https://www.facebook.com/Couture.et.maison" class="social-icon" target="_blank"><i class="icon-facebook"></i></a>
                <a href="#" class="social-icon" target="_blank"><i class="icon-instagram"></i></a>
            </div><!-- End .social-icons -->
        </div><!-- End .footer-bottom -->
    </div><!-- End .containr -->
</footer><!-- End .footer -->