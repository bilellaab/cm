@extends('layouts.layout')

@section('content')

<main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav mb-1">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index-2.html"><i class="icon-home"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">About Us</li>
            </ol>
        </div><!-- End .container -->
    </nav>

    <div class="container">
        <h2 class="light-title rotate-title">
            The New Way to
            <strong class="word-rotater">
                success,advance,progress
            </strong>
        </h2>
        <div class="row">
            <div class="col-lg-10">
                <p class="lead">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non <span class="fancy-text">metus.</span> pulvinar. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh.
                </p>
            </div><!-- End .col-lg-10 -->
            <div class="col-lg-2">
                <a href="#" class="btn btn-primary mt-2">Join Our Team!</a>
            </div><!-- End .col-lg-10 -->
        </div><!-- End .row -->

        <hr>

        <h2 class="light-title text-primary"><strong>WHO</strong> WE ARE</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Nullam convallis, arcu vel pellentesque sodales, nisi est varius diam, ac ultrices sem ante quis sem. Proin ultricies volutpat sapien, nec scelerisque ligula mollis lobortis. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing <span class="fancy-text">metus</span> sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. Fusce eget metus lorem, ac viverra leo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula.</p>

        <hr>

        <h2 class="light-title text-primary">OUR <strong>HISTORY</strong></h2>

        <ul class="history-list">
            <li>
                <div class="thumb">
                    <img src="assets/images/office/office-4.jpg" alt="office">
                </div>
                <div class="featured-box">
                    <div class="box-content">
                        <h3>2018</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,</p>
                    </div>
                </div>
            </li>
            <li>
                <div class="thumb">
                    <img src="assets/images/office/office-3.jpg" alt="office">
                </div>
                <div class="featured-box">
                    <div class="box-content">
                        <h3>2016</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia.</p>
                    </div>
                </div>
            </li>
            <li>
                <div class="thumb">
                    <img src="assets/images/office/office-2.jpg" alt="office">
                </div>
                <div class="featured-box">
                    <div class="box-content">
                        <h3>2010</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,</p>
                    </div>
                </div>
            </li>
            <li>
                <div class="thumb">
                    <img src="assets/images/office/office-1.jpg" alt="office">
                </div>
                <div class="featured-box">
                    <div class="box-content">
                        <h3>2005</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,</p>
                    </div>
                </div>
            </li>
        </ul>
    </div><!-- End .container -->
</main><!-- End .main -->



@endsection