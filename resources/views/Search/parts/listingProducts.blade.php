                    <div class="product">
                        <figure class="product-image-container">
                            <a href="/{{$product['link_rewrite']['language']}}.html" class="product-image">
                                <img src="http://prestashop.test/{{ $product['id_default_image'] }}-medium_default/{{ $product['link_rewrite']['language'] }}.jpg" alt="product">
                            </a>
                            <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                            <a href="product.html" class="paction add-cart" title="Add to Cart">
                                <span>Add to Cart</span>
                            </a>
                            <span class="product-label label-sale">-20%</span>
                        </figure>
                        <div class="product-details product-price-inner">
                            <div class="ratings-container">
                                <div class="product-ratings">
                                    <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                                </div><!-- End .product-ratings -->
                            </div><!-- End .product-container -->
                            <h2 class="product-title">
                                <a href="/{{$product['link_rewrite']['language']}}.html">{{$product['name']['language']}}</a>
                            </h2>
                            <div class="price-box">
                                <span class="old-price hide">{{$product['price']}} </span>
                                <span class="product-price">{{$product['price']}} TND</span>
                            </div><!-- End .price-box -->
                        </div><!-- End .product-details -->
                    </div><!-- End .product -->

