<div class="info-section">
    <div class="container">
        <div class="row">

            <div class="col-md-3">
                <div class="feature-box feature-box-simple text-center">
                    <i class="icon-shipping"></i>

                    <div class="feature-box-content">
                        <h3>Livraison à domicile <span>Gratuite à partir de 300 DT</span></h3>
                    </div><!-- End .feature-box-content -->
                </div><!-- End .feature-box -->
            </div><!-- End .col-md-4 -->

            <div class="col-md-3">
                <div class="feature-box feature-box-simple text-center">
                    <i class="icon-money"></i>

                    <div class="feature-box-content">
                        <h3>PAYEZ à LA LIVRAISON <span>En cash à votre porte</span></h3>
                    </div><!-- End .feature-box-content -->
                </div><!-- End .feature-box -->
            </div><!-- End .col-md-4 -->

      <div class="col-md-3">
                <div class="feature-box feature-box-simple text-center">
                    <i class="icon-earphones-alt"></i>

                    <div class="feature-box-content">
                        <h3>Conseiller client<span>Besoin d'assistance ?</span></h3>
                    </div><!-- End .feature-box-content -->
                </div><!-- End .feature-box -->
            </div><!-- End .col-md-4 -->


            <div class="col-md-3">
                <div class="feature-box feature-box-simple text-center">
                    <i class="icon-ok"></i>

                    <div class="feature-box-content">
                        <h3>Produits de qualité <span>Près de 1500 clients nous font confiance</span></h3>

                    </div><!-- End .feature-box-content -->
                </div><!-- End .feature-box -->
            </div><!-- End .col-md-4 -->
        </div><!-- End .row -->
    </div><!-- End .container -->
</div>