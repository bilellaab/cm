<div class="container">
    <h2 class="subtitle text-left">Informatique | <small>smalltest</small></h2>
    <div class="top-selling-products owl-carousel owl-theme">

        @for ($i = 1; $i < 6; $i++)
        <div class="product">
            <figure class="product-image-container">
                <a href="product.html" class="product-image">
                    <img src="products/{{ $i }}.jpg" alt="product">
                </a>
                <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                <a href="product.html" class="paction add-cart" title="Add to Cart">
                    <span>Add to Cart</span>
                </a>
                <span class="product-label label-sale">-20%</span>
            </figure>
            <div class="product-details product-price-inner">
                <div class="ratings-container">
                    <div class="product-ratings">
                        <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                    </div><!-- End .product-ratings -->
                </div><!-- End .product-container -->
                <h2 class="product-title">
                    <a href="product.html">White Chair</a>
                </h2>
                <div class="price-box">
                    <span class="old-price">$129.00</span>
                    <span class="product-price">$99.00</span>
                </div><!-- End .price-box -->
            </div><!-- End .product-details -->
        </div><!-- End .product -->

        @endfor

    </div><!-- End .featured-proucts -->
</div><!-- End .container -->