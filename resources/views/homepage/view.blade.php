@extends('layouts.layout')

@section('content')
<main class="main">

    @include("HomePage.parts.mainSlider")

    @include("HomePage.parts.mainCategories")

    <div class="mb-2"></div><!-- margin -->
    @include("HomePage.parts.66banners")
    <div class="mb-2"></div><!-- margin -->
    @include("HomePage.parts.condition")
    <div class="mb-2"></div><!-- margin -->
    <hr>
    @include("HomePage.parts.topSellingItems")
    <div class="mb-3"></div><!-- margin -->
    <hr>
    @include("HomePage.parts.topSellingItems")
    <hr>
    <div class="mb-3"></div><!-- margin -->
    @include("HomePage.parts.12banner")

    <div class="mb-3"></div><!-- margin -->
    <hr>
    @include("HomePage.parts.topSellingItems")
    <div class="mb-3"></div><!-- margin -->
    <hr>
    @include("HomePage.parts.topSellingItems")


    <div class="mb-5"></div><!-- margin -->
    <hr>
@include("HomePage.parts.textTest")

</main><!-- End .main -->

@endsection

