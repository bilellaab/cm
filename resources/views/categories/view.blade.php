@extends('layouts.layout')
@section('content')

    <main class="main">

        @include("Categories.parts.categoryBanner")
        @include('Shared.breadcrumb')

        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <nav class="toolbox">
                        <div class="toolbox-left">
                            <div class="toolbox-item toolbox-sort">
                                <div class="select-custom">

                                    <select name="orderby" class="form-control"
                                            data-jplist-control="select-sort"
                                            data-group="group1"
                                            data-name="name1"
                                    >
                                        <option value="0" data-path="default" selected="selected">Trier par: </option>
                                        <option value="popularity">Les plus demandés</option>
                                        <option value="date">Nouvel arrivage</option>
                                        <option value="price"
                                                data-path=".product-price"
                                                data-order="asc"
                                                data-type="number">
                                            Prix croissant</option>
                                        <option value="price-desc"
                                                data-path=".product-price"
                                                data-order="desc"
                                                data-type="number">
                                            Prix décroissant</option>
                                    </select>

                                </div><!-- End .select-custom -->


                            </div><!-- End .toolbox-item -->
                        </div><!-- End .toolbox-left -->
                    </nav>
                    <div class="mb-4"></div><!-- margin -->
                    <div class="row row-sm" data-jplist-group="group1">

                        @if (isset($products) && count ($products)>0)
                            @foreach ($products as $product)
                                <div class="col-6 col-md-4 col-xl-3" data-jplist-item>
                                    @include("Shared.listingProduct")
                                </div><!-- End .col-xl-3 -->
                            @endforeach
                        @else
                            <div class="col-12 col-md-12 col-xl-12"> Pas de Produits :( </div>
                        @endif
                    </div><!-- End .row -->

                    <!-- new pagination-->

                    <nav class="toolbox toolbox-pagination" data-jplist-control="pagination"
                         data-group="group1"
                         data-items-per-page="20"
                         data-current-page="0"
                         data-name="pagination1">
                        <div class="toolbox-item toolbox-show">
                            <label data-type="info"> {startItem} - {endItem} Sur {itemsNumber} Produits trouvés</label>
                        </div><!-- End .toolbox-item -->

                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link page-link-btn" data-type="first" href="#"><i class="icon-angle-left"></i></a>
                            </li>



                            <li class="page-item" data-type="pages">
                                <a data-type="page" class="page-link" href="#" style="margin-right: 1px; margin-left: 1px;">{pageNumber}</a>
                            </li>


                            <li class="page-item" >
                                <a class="page-link page-link-btn" data-type="last" href="#"><i class="icon-angle-right"></i></a>
                            </li>
                        </ul>
                    </nav>


                    <!-- new pagination-->

                </div><!-- End .col-lg-9 -->

                @include('Categories.parts.filters')

            </div><!-- End .row -->
        </div><!-- End .container -->

        <div class="mb-5"></div><!-- margin -->
    </main>

@endsection