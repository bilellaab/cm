<div class="sidebar-overlay"></div>
<div class="sidebar-toggle"><i class="icon-sliders"></i></div>
<aside class="sidebar-product col-lg-3 padding-left-lg mobile-sidebar">
    <div class="sidebar-wrapper">
        <div class="widget widget-brand">
            <a href="#">
                <img src="assets/images/product-brand.png" alt="brand name">
            </a>
        </div><!-- End .widget -->

        <div class="widget widget-info">
            <ul>
                <li>
                    <i class="icon-shipping"></i>
                    <h4>Livraison à domicile</h4>
                    <span>Gratuite à partir de 300 DT</span>
                </li>
                <li>
                    <i class="icon-us-dollar"></i>
                    <h4>PAYEZ à LA LIVRAISON</h4>
                     <span>En cash à votre porte</span>
                </li>
                <li>
                    <i class="icon-ok"></i>
                    <h4>Produits de qualité</h4>
                    <span>Près de 1500 clients nous font confiance</span>
                </li>
            </ul>
        </div><!-- End .widget -->
    </div>
</aside><!-- End .col-md-3 -->