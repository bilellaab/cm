<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomePageController@view')->name('home');
Route::get('/contact', 'PageController@contactUs');
Route::get('/aboutus', 'PageController@aboutUs')->name('about');
Route::get('/cart', 'CartController@view');

Route::get('/testme', 'PageController@testme');

Route::get('/search', 'SearchController@searchResults')->name('searchform');
//Route::get('/search', 'SearchController@view');

Route::get('/checkout', 'CheckoutController@view');
///{productId?}/{$imageId?}/{$size?}
//Route::get('/image', 'ImageController@view');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});




Route::get('/{productSlug}.html', 'ProductController@view');
Route::get('/{categorySlug}', 'CategoryController@view');


