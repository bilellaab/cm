<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\PrestashopRepositoryInterface;

class Category extends Model
{
    private $prestashopRepo;

    public function __construct(PrestashopRepositoryInterface $prestashopRepository)
    {
        $this->prestashopRepo = $prestashopRepository;

    }

    public function wsToArray($response)
    {

            $xml   = simplexml_load_string($response->getBody(), 'SimpleXMLElement', LIBXML_NOCDATA);
            $array = json_decode(json_encode($xml),TRUE);
            $array = array_pop($array);
            $array = array_pop($array);




        if (!isset($array['0']))
        {
            $multiLevelArray['0'] = $array;
            return ($multiLevelArray);
        }
        return ($array);
    }
    private function getCategoryInformations($array)
    {
        return $array;
    }

    private function getSubCategories($array)
    {
        if (!isset($array['0']['associations']['categories']['category']))
        {
            return null;
        }
        $idsCat = $array['0']['associations']['categories']['category'];
        if (count($idsCat)>1) $idsCat = array_column($idsCat, 'id');
        return $idsCat;

        return null;

    }
    public function getRelatedProducts($array)
    {
        //dd($array);
        if (!isset($array['0']['associations']['products']['product']))
        {
            return null;
        }
        $ids = $array['0']['associations']['products']['product'];
        if (count($ids)>1) $ids = array_column($ids, 'id');
        return $ids;
    }





public function getCategoriesByIds($arr)
{
    if (empty($arr)) return null;
    $data = [];
    foreach ($arr as $id)
    {
        $d = $this->getCategoryById($id);
        $data [] = $d['0'];
     }

    return ($data);
}

public function getCategoryById($id)
{
    $ressource = "categories";
    $query = [
        'query' =>
            [
                'display' => 'full',
                'active' => '1',
                'filter' => ['id' => $id]
            ]
    ];
    $response = $this->prestashopRepo->queryPrestashop($ressource, $query);
    $array = $this->wsToArray($response);
    //dd($array);
    //category information
    $categoryInformations = $this->getCategoryInformations($array);
    return $categoryInformations;
}

public function getAllcategoryData($categorySlug)
    {

        $ressource = "categories";
        $query = [
            'query' =>
                [
                    'display' => 'full',
                    'active'  => '1',
                    'filter' => ['link_rewrite'=>$categorySlug]
                ]
        ];
        $response = $this->prestashopRepo->queryPrestashop($ressource,$query);
        $array = $this->wsToArray($response);
        //dd($array);
        //category information
        $categoryInformations = $this->getCategoryInformations($array);
        //dd($categoryInformations);
        $idsRelatedProduct = $this->getRelatedProducts($array);
        //related category
        $idsSubcategories = $this->getSubCategories($array);
        //dd($idsSubcategories);
        return [
            'categoryInformations'=>$categoryInformations,
            'idsRelatedProduct'   =>$idsRelatedProduct,
            'idsSubcategories'    => $idsSubcategories
        ];

    }



}
