<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PrestashopRepositoryInterface;
use App\Product;
use App\Category;

class CategoryController extends BaseController
{

    private $Product;
    private $Category;


    public function __construct(PrestashopRepositoryInterface $prestashopRepository)
    {
        $this->Category = new Category($prestashopRepository);
        $this->Product = new Product($prestashopRepository);
    }
    public function view($categorySlug)
    {
        $data = $this->Category->getAllcategoryData($categorySlug);
        if(!is_array($data ['categoryInformations']['0'])) $this->handle404();
        //dd($data);
        $categoryData = $data ['categoryInformations']['0'];
        $relatedProductsData = $this->Product->getProductsByIds($data['idsRelatedProduct']);
        //dd($relatedProductsData);
        $products = $relatedProductsData['data'];

        $specificPrice = $relatedProductsData ['specificPrice'];
        //dd($specificPrice);
        //dd($categoryData);
        if ($categoryData['id_parent'] > 2) {
            $parentCategory = $this->Category->getCategoryById($categoryData['id_parent']);
            if (!is_array($parentCategory ['0'])) $this->handle404();
            //dd($parentCategory ['0']['id']);
            $breadcrumbs =  [
                '0' => ['title'=> $parentCategory ['0'] ['name']['language'],'url'=>$parentCategory ['0'] ['link_rewrite']['language'] ],
                '1' => ['title'=> $categoryData ['name']['language'],'url'=>$categoryData ['link_rewrite']['language'] ],

                //      '1' => ['title'=> $product ['name']['language'],'url'=>$product ['link_rewrite']['language']],
            ];
        }
        else
        {
            $breadcrumbs =  [
                '0' => ['title'=> $categoryData ['name']['language'],'url'=>$categoryData ['link_rewrite']['language'] ]
                //      '1' => ['title'=> $product ['name']['language'],'url'=>$product ['link_rewrite']['language']],
            ];
        }






        //dd($products);

        $subCategories = $this->Category->getCategoriesByIds($data ['idsSubcategories']);
        //dd($subCategories);
        return (view('categories.view',compact('products','breadcrumbs','specificPrice','categoryData','subCategories')));
    }

}
