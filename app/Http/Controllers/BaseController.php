<?php

namespace App\Http\Controllers;
use Redirect;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    //
    protected function handle404()
    {
        //TODO: redirect to home page instead of 404 page
        abort(404);
        //return Redirect::to('/', 301);
    }
}
