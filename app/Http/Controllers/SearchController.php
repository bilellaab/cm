<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PrestashopRepositoryInterface;

class SearchController extends BaseController
{
    private $prestashopRepo;


    public function __construct(PrestashopRepositoryInterface $prestashopRepository)
    {
        $this->prestashopRepo = $prestashopRepository;
    }


    public function view()
    {


        //return(view("Search.view"));

    }

    public function searchResults (Request $request)
    {
        $str = $request->input('q');
        if (strlen($str)<3) $this->handle404();

        $products = $this->prestashopRepo->searchProducts($str);
        //$categories = $this->prestashopRepo->searchCategories($str);
        $categories = null;
        //dd($productData);
        //if (!is_array($productData)) abort(404);
        //$product = $productData;
        //dd($products);
        //return (view('Products.view',compact('product')));

        //handle empty result
        if (!is_array($products)) $products = null;
        if (!is_array($categories)) $categories = null;



        //dd($categories,$products);
        //dd($products['link_rewrite']['language']);
        return(view("search.view",compact('products','categories')));

    }
}
