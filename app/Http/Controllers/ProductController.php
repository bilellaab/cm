<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Product;
Use App\Category;
use App\Repositories\PrestashopRepositoryInterface;


class ProductController extends BaseController
{
    private $Product;
    private $Category;

    public function __construct(PrestashopRepositoryInterface $prestashopRepository)
    {
       $this->Product = new Product($prestashopRepository);
        $this->Category = new Category($prestashopRepository);
    }

    public function view($productSlug)
    {
        $productData = $this->Product->getBasicProductData($productSlug);
        if (!is_array($productData)) abort(404);
        $product = $productData['0'];
        //dd($product);
        //get Image list
        $images = $this->Product->buildImagesUrl($product);
        $CategoryData = $this->Category->getCategoryById($product['id_category_default']);
        $category = $CategoryData ['0'];
        //dd($category);
        $breadcrumbs =  [
            '0' => ['title'=> $category ['name']['language'],'url'=>$category ['link_rewrite']['language'] ],
            '1' => ['title'=> $product ['name']['language'],'url'=>$product ['link_rewrite']['language']],
        ];
        //get price / reduction
        $specificPrice = $this->Product->getSpecificPrice($product['id'],$product['price']);

        //get stock available


        // linked products
        //$linkedProductsData = $this->Product->getProductsByIds($data['idsLinkedProduct']);

        //dd($category);
        return (view('products.view',compact('product','category','breadcrumbs','images','specificPrice')));
    }

}
