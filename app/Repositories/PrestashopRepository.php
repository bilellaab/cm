<?php

namespace App\Repositories;
use App\Product;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Repositories\PrestashopRepositoryInterface;
use App\Repositories\ProductRepositoryInterface;
use App\Repositories\BaseRepository;

class PrestashopRepository extends BaseRepository implements PrestashopRepositoryInterface
{
    private $client;
    private $Product;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->Product = $productRepository;
        $this->client = new Client(['base_uri' => 'http://cdn.couture-maison.tn/api/',
            'auth' => ['YU7BC1W3H7LI3X5BMLN59TCARPURMXUK', '']
        ]);
    }

    public function searchCategories($str)
    {
        $ressource ="categories";
        $query = [
            'query' =>
                [
                    'display' => 'full',
                    'filter[name]' => "%[$str]%",
                    'filter[active]' => '[1]'
                ]
        ];
        $response = $this->queryPrestashop($ressource,$query);
        $array = $this->toArraySearch($response);
        return $array;
    }
    public function searchProducts($str)
    {
        $ressource ="products";
        $query = [
            'query' =>
                [
                    'display' => 'full',
                    'filter[name]' => "%[$str]%",
                    'filter[active]' => '[1]'
                ]
        ];
        $response = $this->queryPrestashop($ressource,$query);
        $array = $this->toArraySearch($response);
        //TODO fixe price format
        return $array;
    }


    public function queryPrestashop($ressource,$query)
    {
        $response = $this->client->get($ressource,$query);
        if (empty($response)) return null;
        return $response;
    }




    public function toArraySearch($response)
    {
        $xml   = simplexml_load_string($response->getBody(), 'SimpleXMLElement', LIBXML_NOCDATA);
        $array = json_decode(json_encode($xml),TRUE);
        $array = array_pop($array);
        $array = array_pop($array);

        if (!isset($array['0']))
        {
            $multiLevelArray['0'] = $array;
            return ($multiLevelArray);
        }
        return ($array);
    }










    /*
public function getAllProductsId(){
        $response = $this->client->get('products' );
        $arr =  $this->ProductToArray($response);
        return ($arr);
    }
    public function getProductsByCategory($cat)
    {
        $response = $this->client->get(
            'categories',
            [
                'query' =>
                    [
                        'display' => 'full',
                        'filter' => ['link_rewrite'=>$cat]
                         ]
            ]
        );
        $arr =  $this->CategoryToArray($response);
        //get products information

    }
*/


    /*
public function getProductBySlug($slug)
{
    $response = $this->client->get(
        'products',
        [
            'query' =>
                [
                    'display' => 'full',
                    'filter' => ['link_rewrite'=>$slug]
                ]
        ]
    );
    $array = $this->ProductToArray($response);
    return $array;
}
*/

}