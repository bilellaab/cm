<?php

namespace App;
use App\Category;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\PrestashopRepositoryInterface;

class Product extends Model
{

    private $prestashopRepo;
    private $Category;

    public function __construct(PrestashopRepositoryInterface $prestashopRepository)
    {
        $this->prestashopRepo = $prestashopRepository;
        $this->Category = new category($prestashopRepository);

    }






    public function getProductById($id)
    {
        $ressource = "products";
        $query = [
            'query' =>
                [
                    'display' => 'full',
                    'active'  => '1',
                    'filter' => ['id'=>$id]
                ]
        ];
        $response = $this->prestashopRepo->queryPrestashop($ressource,$query);
        $array = $this->wsToArray($response);
        $array = $this->formatProductArray($array);
        return $array;
    }
    public function getBasicProductData($productSlug)
    {
        $ressource = "products";
        $query = [
            'query' =>
                [
                    'display' => 'full',
                    'filter' => ['link_rewrite'=>$productSlug],
                    'filter[active]' => '[1]'
                ]
        ];
        $response = $this->prestashopRepo->queryPrestashop($ressource,$query);
        $array = $this->wsToArray($response);
        $productData = $this->formatProductArray($array);
        return $productData;
    }
    public function buildImagesUrl($product)
    {
        if (!is_array($product['associations']['images']['image']))
            return null;

        $ids = $product['associations']['images']['image'];

        $url_prestashop = config('prestashop-webservice.url');
        //dd($url_prestashop);
        $protocol = config('prestashop-webservice.protocol');
        $images = [];

        if (count($ids)>1) $ids = array_column($ids,'id');

        foreach ($ids as $id)
        {
            $images['large'][] = $protocol.$url_prestashop."/".$id."-large_default/".$product['link_rewrite']['language'].".jpg";
            $images['small'][] = $protocol.$url_prestashop."/".$id."-small_default/".$product['link_rewrite']['language'].".jpg";
        }
        //http://prestashop.test/{{ $product['id_default_image'] }}-large_default/{{ $product['link_rewrite']['language'] }}.jpg
        return $images;

    }
    private function formatProductArray($array)
    {
        if (empty($array)) return null;

        //unset unused information

        //format inital price
        $data = [];
        foreach ($array as $product)
        {
            $formattedPrice = number_format($product['price'], 0, ',', ' ');
            $product['price'] = $formattedPrice;
            $data [] = $product;

        }
        return ($data);
    }

    public function getProductByIdsOneCall($arr)
    {
        //if (empty($arr)) return null;
        $specificPrice =[];
        $data = [];
        $info = [];
        $str='';
        foreach ($arr as $r)
        {
           if ($r === reset($arr))
               $str = $r;
           else
            $str = $str.'|'.$r;
        }

        $ressource = "products";
        $query = [
            'query' =>
                [
                    'display' => 'full',
                    'active'  => '1',
                    'filter[id]' => "[$str]"
                ]
        ];


        $response = $this->prestashopRepo->queryPrestashop($ressource,$query);
        $array = $this->wsToArray($response);
        return $array;

    }

    public function getProductsByIds ($arr)
    {
        if (empty($arr)) return null;
        $specificPrice =[];
        $data = [];
        $info = [];
        foreach ($arr as $id)
        {
            $d = $this->getProductByID($id);
            $data [] = $d['0'];
            // prices
            $specificPrice [$d['0']['id']] = $this->getSpecificPrice($d['0']['id'],$d['0']['price']);
        }
        $info = ['data'=>$data,'specificPrice'=>$specificPrice ];
        return ($info);

    }
    public function getLinkedProducts($array)
    {
        if (!isset($array['0']['associations']['accessories']['product']))
        {
            return null;
        }
        $ids = $array['0']['associations']['products']['product'];
        if (count($ids)>1) $ids = array_column($ids, 'id');
        return $ids;
    }
    public function getSpecificPrice($id,$oldPrice)
    {
        $ressource = "specific_prices";
        $query = [
            'query' =>
                [
                    'display' => 'full',
                    'filter' => ['id_product'=>$id],
                ]
        ];
        $response = $this->prestashopRepo->queryPrestashop($ressource,$query);
        $array = $this->wsToArray($response);
        if (!is_array($array['0'])) return null;

        //dd ($array);
        $array = $array['0'];
        $newPrice = $percentage =  null;
        $ret =[];
        if ($array['reduction_type'] == 'percentage')
        {
            $newPrice = $oldPrice - ($oldPrice * $array['reduction']);
            $percentage = '-'. ($array['reduction'] * 100) .'%';
            $ret = [
                'newPrice' => $newPrice,
                'percentage' => $percentage
            ];
        }

        return ($ret);
}



































    public function wsToArray($response)
    {
        $xml   = simplexml_load_string($response->getBody(), 'SimpleXMLElement', LIBXML_NOCDATA);
        $array = json_decode(json_encode($xml),TRUE);
        $array = array_pop($array);
        $array = array_pop($array);

        if (!isset($array['0']))
        {
            $multiLevelArray['0'] = $array;
            return ($multiLevelArray);
        }
        return ($array);
    }
}
